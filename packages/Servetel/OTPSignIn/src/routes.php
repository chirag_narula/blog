<?php
/**
 * Created by PhpStorm.
 * User: chirag.narula
 * Date: 07-02-2019
 * Time: 12:33
 */

Route::get('otp', function(){
    echo 'Hello from the OTP package!';
});

Route::get('add/{a}/{b}', 'Servetel\OTPSignIn\OTPController@add');
Route::get('subtract/{a}/{b}', 'Servetel\OTPSignIn\OTPController@subtract');