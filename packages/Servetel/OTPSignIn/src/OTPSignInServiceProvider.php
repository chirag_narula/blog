<?php

namespace Servetel\OTPSignIn;

use Illuminate\Support\ServiceProvider;

class OTPSignInServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->make('Servetel\OTPSignIn\OTPController');
        $this->loadViewsFrom(__DIR__.'/views', 'OTPSignIn');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
    }
}
